# Autoscaling
* Create Deployment with resources requests cpu 200m

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
    name: hpa-example
spec:
    replicas: 1
    template:
        metadata:
            name: hpa-example
            labels:
                app: hpa-example
        spec:
            containers:
            - name: hpa-example
              image: bankwing/hpa-example
              ports:
              - name: http-port
                containerPort: 80
              resources:
                  requests:
                      cpu: 200m
```

* Create LoadBalancer service

```
---
apiVersion: v1
kind: Service
metadata:
    name: hpa-example-svc
spec:
    type: NodePort
    selector:
        app: hpa-example
    ports:
    - port: 80
      nodePort: 30000
      targetPort: http-port
      protocol: TCP
```

* Create autoscaling

```
---
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
    name: hpa-example-autoscaler
spec:
    scaleTargetRef:
        apiVersion: extensions/v1beta1
        kind: Deployment
        name: hpa-example
    minReplicas: 1
    maxReplicas: 10
    targetCPUUtilizationPercentage: 50
```

* Load test

```
while true; do curl 127.0.0.1:30000 > /dev/null; echo; done
```