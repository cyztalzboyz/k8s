# Create a ConfigMap from a Configuration File
* First, we need to create a configuration file. We can have a configuration file with the content like:

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: customer-ibm
data:
  TEXT1: Customer1_Company
  TEXT2: Welcomes You
  COMPANY: IBM Solutions Delivery Co., Ltd.
```

* If we name the file with the configuration above as configmap.yaml, we can then create the ConfigMap with the following command:

```
kubectl create -f configmap.yaml
```

# Use ConfigMap Inside Pods

```
....
 containers:
      - name: rsvp-app
        image: teamcloudyuga/rsvpapp
        env:
        - name: MONGODB_HOST
          value: mongodb
        - name: TEXT1
          valueFrom:
            configMapKeyRef:
              name: customer-ibm
              key: TEXT1
        - name: TEXT2
          valueFrom:
            configMapKeyRef:
              name: customer-ibm
              key: TEXT2
        - name: COMPANY
          valueFrom:
            configMapKeyRef:
              name: customer-ibm
              key: COMPANY

....
```